#Frontend
Get into the 'frontend' directory and open the index.html file with a browser.
No need to install dependencies, just internet connection to get dependencies from CDN's.

#Backend
Get into the 'backend' directory:

##Install required dependencies
```
sudo pip install -r requirements.txt
```

##Running backend, execute:
```
python app.py
```

Tested with python version: 3.8.1

The backend will listen on port 5000

##Testing backend, execute:
```
pytest
```

##Generate API documentation
Install required dependency: apidoc (if necessary install npm). Execute:
```
sudo npm install apidoc -g
```

Generate apidoc directory automatically with expected documentation in the parent folder:

```
cd ..
```

```
apidoc -i backend/ -o apidoc
```


##PROJECT STRUCTURE
A functional structure it's been chosen for the project structure.
https://www.freecodecamp.org/news/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563/

The app.py file runs the server.
In the uniqueness folder the files:

* controller.py defines the endpoint
* service.py holds the business logic
* test_service for unit testing
* test_controller for integration testing (To fix it)