//Enviroment variables should be loaded directlly from files instead from window variable.
//But that would require to lunch the web with a web server to serve the files.
//To keep it simple to execute, lets do it this way.
window.env = {
    dev : {
        apiUrl : "http://localhost:5000",
    }
}