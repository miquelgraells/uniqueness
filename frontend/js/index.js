$( document ).ready(function () {
    initVue();
})

function initVue() {
    //Languages literal should be loaded directly from files instead from window variable.
    //But that would require to lunch the web with a web server to serve the files.
    //To keep it simple to execute, lets do it this way.
    let literals = window.lang;

    const i18n = new VueI18n({
        locale: 'en',
        literals, 
    })

    new Vue({
        i18n,
        el: '#app',
        data: {
          uniquenessPercentage: '',
          texts: ['Alice was beggining to get very tired', 'he was beggining to get the point'],
          nGrams: 4,
          doingRequest: false,
          infoMessage: '',
          errorMessage: '',
          hightlightstexts: []
        },
        methods: {
            isAllTextsFilled: function () {
                for (let i = 0; i < this.texts.length; i++) {
                    if (this.texts[i].length === 0) {
                        return false;
                    }
                }
                return true;
            },
            addText: function () {
                if (this.isAllTextsFilled()) {
                    this.texts.push('');
                } else {
                    this.infoMessage = window.lang[i18n.locale]['pleaseFillAllTexts'];
                }
            },
            removeText: function (index) {
                if (this.texts.length > 2) {
                    this.texts.splice(index,1);
                } else {
                    this.infoMessage = window.lang[i18n.locale]['minimum2TextsToCompare'];
                }
                
            },
            isValidNGrams: function () {
              return this.nGrams >= 1;  
            },
            isValidFields: function () {
                return this.isAllTextsFilled() && this.isValidNGrams();
            },
            uniquenessRequest: function () {
                if (this.isValidFields()) {
                    this.doingRequest = true;
                    let data = {
                        texts : this.texts,
                        nGrams: this.nGrams
                    }
                    var config = {
                        headers: {
                            'Access-Control-Allow-Origin': '*',
                            'Access-Control-Allow-Headers': 'Access-Control-Allow-Methods, Access-Control-Allow-Origin, Origin, Accept, Content-Type',
                            'Content-Type': 'application/json',
                            'Accept': 'application/json'
                           }
                    };
                    axios.post(window.env.dev.apiUrl + '/uniqueness', data, config)
                    .then((response) => { 
                        this.doingRequest = false;
                        this.uniquenessResponse(response)
                    })
                    .catch((err) => {
                        console.log(err)
                        this.doingRequest = false;
                        this.errorMessage = window.lang[i18n.locale]['somethingWentWrong'];
                    })
                } else {
                    this.infoMessage = window.lang[i18n.locale]['pleaseCheckAllFieldsAreCorrect'];
                }
            },
            uniquenessResponse: function (response) {
                if (response.status == 200) {
                    this.uniquenessPercentage = "" + response.data.uniqueness;
                    this.setHeighlightTexts(response.data.commmonNGrams)
                } else {
                    this.errorMessage = window.lang[i18n.locale]['somethingWentWrong'];
                }                
            },
            setHeighlightTexts: function (commmonNGrams) {
                this.hightlightstexts = [];
                let toHightlight = this.texts;
                for (let i = 0; i < toHightlight.length; i++) {
                    let textHighlights = []; 
                    for (let e = 0; e < commmonNGrams.length; e++) {
                        let textToHightlight = toHightlight[i];
                        let nGram = commmonNGrams[e];

                        if (textToHightlight.indexOf(nGram) > -1) {
                            let heigh = "<span class='highlight'>" + nGram + "</span>";                           
                            // replace all ocurrences
                            var replace = new RegExp(nGram,"g");
                            let heighlightText = textToHightlight.replace(replace, heigh) 
                            textHighlights.push(heighlightText);    
                        }
                        
                    }
                    this.hightlightstexts.push(textHighlights);
                }
            }
        }
    })
}
