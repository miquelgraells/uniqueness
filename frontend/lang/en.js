window.lang = {
    en : {
        pleaseFillAllTexts : "Please fill all texts",
        minimum2TextsToCompare: "Minimum 2 texts to compare",
        somethingWentWrong: "Something went wrong",
        pleaseCheckAllFieldsAreCorrect: "Please check all fields are correct",
        textUniqueness : "Text uniqueness",
        textToCompare: "Texts to compare",
        text: "Text",
        remove: "Remove",
        addText: "Add text",
        nGrams: "N Grams",
        compare: "Compare",
        commonNGramsHighlights: "Common Ngrams highlights",
        nGram: "Ngram",
        uniqueness: "Uniqueness"
    }
}

