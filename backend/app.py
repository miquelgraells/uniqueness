from flask import Flask, Blueprint
from flask_restful import Api
from uniqueness.controller import Uniqueness
from flask_cors import CORS

app = Flask(__name__)
CORS(app)
api = Api(app)

api.add_resource(Uniqueness, '/uniqueness')

if __name__ == '__main__':
    app.run(debug=True)