import pytest
from service import Service


def test_getUniquenesPersentage():
    texts = ["Alice was beginning to get very tired","He was beginning to get the point"]
    nGrams = 4

    service = Service(texts, nGrams)
    uniqueness = service.getUniquenesPersentage()

    expected = 85.7 

    assert expected == uniqueness
    
def test_getNGramsList():
    texts = ["Alice was beginning to get very tired","He was beginning to get the point"]
    nGrams = 4

    service = Service(texts, nGrams)
    nGramsList = service.getNGramsListsByTexts()

    expected = [
        ['Alice was beginning to', 'was beginning to get', 'beginning to get very', 'to get very tired'], 
        ['He was beginning to', 'was beginning to get', 'beginning to get the', 'to get the point']
    ]

    assert expected == nGramsList


def test_getuniqueNGramsInEitherTexts():
    texts = ["Alice was beginning to get very tired","He was beginning to get the point"]
    nGrams = 4

    service = Service(texts, nGrams)
    listNGramsTexts = service.getNGramsListsByTexts()
    nGramsList = service.getuniqueNGramsInEitherTexts(listNGramsTexts)

    expected = ['beginning to get the', 'to get the point', 'was beginning to get', 'to get very tired', 'beginning to get very', 'He was beginning to', 'Alice was beginning to']
    expected.sort()
    nGramsList.sort()
    assert expected == nGramsList

def test_getuniquesNGramsInAllTexts():
    texts = ["Alice was beginning to get very tired","He was beginning to get the point"]
    nGrams = 4

    service = Service(texts, nGrams)
    listNGramsTexts = service.getNGramsListsByTexts()
    nGramsList = service.getuniquesNGramsInAllTexts(listNGramsTexts)

    expected = ['was beginning to get']
    expected.sort()
    nGramsList.sort()

    assert expected == nGramsList   

def test_validateTexts_MinimumTwoTextsrequired():
    texts = ["Alice was beginning to get very tired"]

    expected = "Minimum two texts required" 

    result = None

    try:
        Service.validateTexts(texts)
    except Exception as e:
        result = str(e)

    assert expected == result

def test_validateTexts_EmptyTextsNotValid():
    texts = ["Alice was beginning to get very tired", "He was beginning to get the point", ""]

    expected = "Empty texts not valid" 

    result = None

    try:
        Service.validateTexts(texts)
    except Exception as e:
        result = str(e)

    assert expected == result

def test_validateTexts_NoException():
    texts = ["Alice was beginning to get very tired", "He was beginning to get the point"]

    result = None

    try:
        Service.validateTexts(texts)
    except Exception as e:
        result = str(e)

    assert result is None