from flask_restful import Resource, reqparse, inputs
from flask import request, jsonify
from uniqueness.service import Service, ExceptionCanNotCalculateBySentensesAndNgram

class Uniqueness(Resource):
    """
    @api {post} /uniqueness Request uniqueness percentatge and common n-grams of a list of sentenses
    @apiName PostUniqueness
    @apiGroup Uniqueness

    @apiParam {Number} nGrams N grams to calculate the uniqueness. It has to be bigger than 0.
    @apiParam {List} texts List of texts to calculate their uniqueness given the nGrams parameter.

    @apiParamExample {json} Request-Example:
        {
	        "nGrams" : "4",
	        "texts" : ["Alice was beggining to get very tired", "he was beggining to get the point"]
        }

    @apiSuccessExample {json} Success-Response:
        HTTP/1.1 200 OK
        {
            "commmonNGrams": [
                "was beggining to get"
            ],
            "uniqueness": 85.7
        }

    @apiErrorExample Error-Response:
        HTTP/1.1 400 BAD REQUEST
        {
            "message": {
                "texts": "Minimum two texts required"
            }
        }
    @apiErrorExample Error-Response:
        HTTP/1.1 400 BAD REQUEST
        {
            "message": {
                "texts": "Empty texts not valid"
            }
        }
    @apiErrorExample Error-Response:
        HTTP/1.1 400 BAD REQUEST
        {
            "message": {
                "texts": "Check nGrams param is correct"
            }
        }
    """
    def post(self):
        parser = reqparse.RequestParser(bundle_errors=True)
        parser.add_argument('nGrams', required=True, type=inputs.positive, help="Check nGrams param is correct")
        parser.add_argument('texts', required=True, type=Service.validateTexts, location='json')
        parser.parse_args()

        try:
            args = request.get_json()
            texts = args['texts']
            nGrams = int(args['nGrams'])
            
            service = Service(texts, nGrams)
            uniqueness = service.getUniquenesPersentage()
            listNGramsTexts = service.getNGramsListsByTexts()
            commmonNGrams = service.getuniquesNGramsInAllTexts(listNGramsTexts)
            return jsonify(
                uniqueness = uniqueness,
                commmonNGrams = commmonNGrams
            )
        except ExceptionCanNotCalculateBySentensesAndNgram:
            return jsonify(
                uniqueness = 0,
                commmonNGrams = []
            )
        except:
            return jsonify(
                message = 'Internal Error'
            )
        