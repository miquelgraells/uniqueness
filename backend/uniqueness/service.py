from nltk.util import ngrams

class ExceptionCanNotCalculateBySentensesAndNgram(Exception):
    pass

class Service():
    def __init__(self, texts, n):
        self.texts = texts
        self.n = n

    @staticmethod
    def validateTexts(texts):
        if len(texts) < 2:
            raise ValueError('Minimum two texts required')
        else:
            for text in texts:
                if not text:
                    raise ValueError('Empty texts not valid')
            return texts

    def getUniquenesPersentage(self):
        listNGramsTexts = self.getNGramsListsByTexts()
        uniquesNGramsInEitherTexts = self.getuniqueNGramsInEitherTexts(listNGramsTexts)
        uniquesNGramsInAllTexts = self.getuniquesNGramsInAllTexts(listNGramsTexts)
        if len(uniquesNGramsInEitherTexts) > 0:
            result = (1 - len(uniquesNGramsInAllTexts) / len (uniquesNGramsInEitherTexts)) * 100
            return round(result, 1)
        else:
            raise ExceptionCanNotCalculateBySentensesAndNgram
    
    def getNGramsListsByTexts(self):
        result = []
        separator = ' '
        for text in self.texts:
            tokens = [token for token in text.split(" ") if token != ""]
            x = ngrams(tokens, self.n)
            nGramsList = list(x)
            nGramsTextList = []
            for ngramList in nGramsList:
                ngramList = separator.join(ngramList)
                nGramsTextList.append(ngramList)
            result.append(nGramsTextList)
        return result

    def getuniqueNGramsInEitherTexts(self, listNGramsTexts):
        result = []
        for text in listNGramsTexts:
            for nGram in text: 
                result.append(nGram)

        result = list(set(result))
        return result

    def getuniquesNGramsInAllTexts(self, listNGramsTexts):
        result = []        
        nGramsFirstText = listNGramsTexts[0]
        for nGram in nGramsFirstText:
            for index, nGramList in enumerate(listNGramsTexts):
                if not nGram in nGramList:
                    break
                elif index+1 == len(listNGramsTexts): #if last row
                    result.append(nGram)
        return result
        